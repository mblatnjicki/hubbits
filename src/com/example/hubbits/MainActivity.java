package com.example.hubbits;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity {

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		String token = "";
		String iid = InstanceID.getInstance(this).getId();//.getInstance(context).getId();
		String authorizedEntity = "264792827666"; // Project id from Google Developer Console
		String scope = "GCM"; // e.g. communicating using GCM, but you can use any
		                      // URL-safe characters up to a maximum of 1000, or
		                      // you can also leave it blank.
		try {
			token = InstanceID.getInstance(this).getToken("264792827666", GoogleCloudMessaging.INSTANCE_ID_SCOPE);
			TextView lt = (TextView)findViewById(R.id.lblRegistration);
			lt.setText(token);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findViewById(R.id.lblRegistration).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				//Intent i = new Intent(getApplicationContext(), RegistrationActivity.class);
				//startActivity(i);
			}
		});
		
		findViewById(R.id.btnFacebook).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
				startActivity(i);
			}
		});
		
		findViewById(R.id.btnTwitter).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
				startActivity(i);
			}
		});
		
		findViewById(R.id.btnInstagram).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
				startActivity(i);
			}
		});
		
		//GcmIntentService gis = new GcmIntentService();
		//gis.st
	}
}
