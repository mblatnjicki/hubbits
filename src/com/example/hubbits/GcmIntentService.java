package com.example.hubbits;

import com.google.android.gms.gcm.GcmListenerService;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

public class GcmIntentService extends GcmListenerService {
	    @Override
	    public void onMessageReceived(String from, Bundle data) {
	        String message = data.getString("message");
	        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
	                .setSmallIcon(R.drawable.ic_launcher)
	                .setContentTitle("Test")
	                .setContentText(message);
	        notificationManager.notify(1, mBuilder.build());
	    }
	}