package com.example.hubbits;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class RegistrationActivity extends Activity {

	protected static final String USER_AGENT = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		
		findViewById(R.id.btnConfirmRegistration).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				RegistrationForm rf = new RegistrationForm();
				TextView tvEmail = (TextView)findViewById(R.id.tvEmail);
				rf.email = tvEmail.getText();
				TextView tvName = (TextView)findViewById(R.id.tvName);
				rf.name = tvName.getText();
				TextView tvLastname = (TextView)findViewById(R.id.tvLastname);
				rf.lastname = tvLastname.getText();
				TextView tvCreditCardNumber = (TextView)findViewById(R.id.tvCreditCardNumber);
				rf.creditCardNumber = tvCreditCardNumber.getText();
				
				String url = "https://localhost/put/user";
				URL obj = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

				//add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", USER_AGENT);
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

				String urlParameters = "email="+rf.email+"name="+rf.name+"lastname="+rf.lastname+"creditCardNumber="+rf.creditCardNumber;
				
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();

				int responseCode = con.getResponseCode();

				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
				startActivity(i);
			}
		});
	}
}
